FROM registry.gitlab.com/mrponpon/ocr-dhl-ai:env_base
WORKDIR /ai
ADD . /ai
RUN python -m pip install .

CMD uvicorn app:app --host "0.0.0.0" --port "5000" --workers 2 --reload --debug