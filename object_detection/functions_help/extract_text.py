# Import packages
import os
import cv2
import numpy as np
import sys
from PIL import Image

# This is needed since the notebook is stored in the object_detection folder.
# sys.path.append("..")
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import json
import base64
import time
import re
import requests
import easyocr
from .get_image_box import get_image_list

reader = easyocr.Reader(['en'], gpu = False,model_storage_directory = "object_detection/craft_models")

tesseract_ocr = os.getenv('tesseract_ocr')
craft_ocr = os.getenv('craft_ocr')
ip_topsarun_ocr = os.getenv("api_ocr")
manageai_key = os.getenv("manageai_key")
tap_right_percent = os.getenv("tap_right_percent",default = 15)
tap_left_percent = os.getenv("tap_left_percent",default = 5)
print(ip_topsarun_ocr,manageai_key)
ip_tesseract = f'http://{tesseract_ocr}'
ip_craftocr = f'http://{craft_ocr}'

def regex_date(text):
    match = re.search(r'(\d+/\d+/\d+)',text)
    return match

def regex_number(text):
    match = re.sub('[^+X0-9]', '', text)
    return match

def currency_format_f2(text):
    # print("input currency",text)
    text = text.replace("o","0").replace("O","0").replace("l","1").replace("+","")
    list_text = text.split(".")
    if len(list_text[-1]) > 2:
        temp_list_text = list_text[:len(list_text)-1]
        temp_list_text = "".join(temp_list_text)
        temp = list_text[-1][:2]
        text = f"{temp_list_text}.{temp}"
    
    text = regex_number(text)
    if len(text) >= 3:
        text = f"{text[:len(text)-2]}{'.'}{text[len(text)-2:]}"
        # print(text)
        if float(text) > 0.0:
            text = "-{:,.2f}".format(float(text))
        else:
            text = "{:,.2f}".format(float(text))

    return text

def regex_number_letter(text):
    match = re.sub(r'^\s+|\s+$', '', text)
    match = match.replace(" ","_")
    match = re.sub(r'[^A-Za-z0-9_/-]', '', match)
    match = match.replace("_"," ")
    if len(match) > 1:
        if match[0] == " ":
            match = match[1:]
    return match
    
def creditnote_pattern(text):
    text = regex_number_letter(text)
    text_list = text.split()
    if len(text_list) > 1:
        text_list[0] = text_list[0].replace("o","0").replace("O","0").replace("l","1")
        if len(text_list[1]) > 3:
            temp = text_list[1][2:len(text_list[1])].replace("o","0").replace("O","0").replace("l","1")
            text_list[1] = f"{text_list[1][:2]}{temp[0]}{temp[-1]}"
        text = ' '.join(text_list)
        # print(text)

    return text

def detect_text(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    sobel = cv2.Sobel(img, cv2.CV_8U, 1, 0, ksize=3)
    ret, binary = cv2.threshold(sobel, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)
    element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (20, 5))
    dilation = cv2.dilate(binary, element2, iterations=1)
    region = []
    contours, _ = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    if contours:
        (contours, boundingBoxes) = sort_contours(contours,method ="left-to-right")
        return contours,boundingBoxes
    else:
        return False,False

# def easyocr_reader(image):
#     result = reader.readtext(image)
#     result_list = []
#     for r in result:
#         if r[2] > 0.1:
#             result_list.append(r[1])
#     result_text  = ' '.join(result_list)
#     return result_text

def ocr_topsarun_api_longtext(img,min_size=20,text_threshold = 0.7,low_text = 0.3,link_threshold = 0.4,is_bbox=False):
    result = reader.detect(img,min_size=min_size,text_threshold = text_threshold,low_text = low_text,link_threshold=link_threshold)
    horizontal_list, free_list = result
    if (horizontal_list==None) and (free_list==None):
        y_max, x_max = img_cv_grey.shape
        horizontal_list = [[0, x_max, 0, y_max]]
        free_list = []
    text_list = [] 
    for bbox in horizontal_list:
        h_list = [bbox]
        f_list = []
        image_list = get_image_list(h_list, f_list, img)
        for im in image_list:
            result0 = ocr_topsarun_api(im[1])
            if is_bbox:
                text_list.append((im[0],result0))
            else:
                text_list.append(result0)

    if not is_bbox:
        text_list = " ".join(text_list)
    # print(len(horizontal_list),"\t",text_list)
    return text_list

def ocr_topsarun_api(image):
    retval, buffer = cv2.imencode('.jpg', image)
    img_base64 = base64.b64encode(buffer).decode('utf-8')
    headers = {"manageai-key":str(manageai_key)}
    data = {
        "base64img": img_base64
    }
    r = requests.post(str(ip_topsarun_ocr), json = data,headers=headers,verify=False)
    result = r.json()
    if result !=None:
        return result["prediction"][0]["text"]
    else:
        return ""

def tesseract_api(image,ratio, _lang="tha+eng", mode='11'):
    # if ratio > 0:
    #     image = resize_box(image,ratio)
    retval, buffer = cv2.imencode('.jpg', image)
    img_base64 = base64.b64encode(buffer).decode('utf-8')
    data = {
        "psm": mode,
        "lang": _lang,
        "_file": img_base64
    }
    r = requests.post(str(ip_tesseract), json = data,verify=False)
    result = r.json()

    return result["result"]

def craftocr_api(image):
    retval, buffer = cv2.imencode('.jpg', image)
    img_base64 = base64.b64encode(buffer).decode('utf-8')
    data = {
        "image_base64": img_base64
    }
    r = requests.post(str(ip_craftocr), json = data,verify=False)
    result = r.json()

    return result["result"]

def sort_contours(cnts, method="left-to-right"):
    try:
        reverse = False
        i = 0
        if method == "right-to-left" or method == "bottom-to-top":
            reverse = True
        if method == "top-to-bottom" or method == "bottom-to-top":
            i = 1
        boundingBoxes = [cv2.boundingRect(c) for c in cnts]
        (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes), key=lambda b: b[1][i], reverse=reverse))   
    except Exception as e:
        print(e,cnts)

    return (cnts, boundingBoxes)
    
def denoise(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    kernel = np.ones((2, 2), np.uint8)
    img = cv2.dilate(img, kernel, iterations=1)
    img = cv2.erode(img, kernel, iterations=1)
    return img

def get_box(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    (thresh, img_bin) = cv2.threshold(img, 128, 255,
                                      cv2.THRESH_BINARY | cv2.THRESH_OTSU)  # Thresholding the image
    img_bin = 255-img  # Invert the image
    
    kernel_length = np.array(img).shape[1]//40
     
    verticle_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, kernel_length))
    hori_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_length, 1))
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))

    img_temp1 = cv2.erode(img_bin, verticle_kernel, iterations=3)
    verticle_lines_img = cv2.dilate(img_temp1, verticle_kernel, iterations=3)
    # cv2.imwrite("verticle_lines.jpg",verticle_lines_img)

    img_temp2 = cv2.erode(img_bin, hori_kernel, iterations=3)
    horizontal_lines_img = cv2.dilate(img_temp2, hori_kernel, iterations=3)
    # cv2.imwrite("horizontal_lines.jpg",horizontal_lines_img)

    alpha = 0.5
    beta = 1.0 - alpha
    img_final_bin = cv2.addWeighted(verticle_lines_img, alpha, horizontal_lines_img, beta, 0.0)
    img_final_bin = cv2.erode(~img_final_bin, kernel, iterations=2)

    (thresh, img_final_bin) = cv2.threshold(img_final_bin, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    contours, hierarchy = cv2.findContours(img_final_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    return contours

def pre_auto_row(img, morph_size=(5,5), threshold=4):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    threshed = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    cpy = threshed.copy()
    struct = cv2.getStructuringElement(cv2.MORPH_RECT, morph_size)
    cpy = cv2.dilate(~cpy, struct, anchor=(-1, -1), iterations=1)
    hist = cv2.reduce(cpy,1, cv2.REDUCE_AVG).reshape(-1)

    th = threshold
    H,W = img.shape[:2]
    lowers = [y for y in range(H-1) if hist[y]>th and hist[y+1]<=th]
    return H,W,lowers

def normalized_coor(xmin,xmax,ymin,ymax,im_width,im_height):
    (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                ymin * im_height, ymax * im_height)
    return int(left), int(right), int(top), int(bottom)

def resize_box(crop_img,ratio):
    model_height = 64
    crop_img = cv2.resize(crop_img, (int(model_height*ratio), model_height), interpolation =  Image.ANTIALIAS)
    return crop_img

def extract_text(image_copy,box,lable_string,height, width):
    ymin, xmin, ymax, xmax = box
    left, right, top, bottom = normalized_coor(xmin,xmax,ymin,ymax,width,height)
    width = right - left 
    height = bottom - top
    ratio = width/height
    tap_right = 20
    tap_left = 20
    if lable_string != 'table':
        tap_right = int(width*int(tap_right_percent)/100)
        tap_left = int(width*int(tap_left_percent)/100)
    try:
        crop_img = image_copy[int(top):int(bottom)+2, max(int(left)-tap_left,0):int(right)+tap_right]
        # cv2.imwrite(f"test_dhl/{str(lable_string)}.jpg",crop_img)
    except:
        crop_img = image_copy[int(top):int(bottom), int(left):int(right)]
        # cv2.imwrite(f"test_dhl/{str(lable_string)}.jpg",crop_img)
    result_dict0 = {}
    if lable_string == 'addr':
        result_list1 = []
        result_dict1 = {}
        result_bbox = ocr_topsarun_api_longtext(crop_img,is_bbox=True)
        # result_bbox = reader.readtext(crop_img)
        n1 = 0
        for r in result_bbox:
            # print(f"=={n1}==",r)
            n1 += 1
            text = r[1].replace("_","").upper()
            bbox = r[0]
            xmin = int(bbox[0][0])
            xmax = int(bbox[2][0])
            ymin = int(bbox[0][1])
            ymax = int(bbox[2][1])
            # new_img = crop_img[ymin:ymax,xmin:xmax]
            #cv2.imwrite(f'test_dhl/bbox_{lable_string}{n1}.jpg',new_img)
            if n1 == len(result_bbox):
                result_dict0["postcode"] = text
            else:
                this_key = ymin
                if this_key in result_dict1:
                    result_dict1[this_key].append(text)
                else:
                    for k in result_dict1:
                        if this_key in range(max(0,k-10),k+10):
                            this_key = k
                            break
                    if this_key == ymin:
                        result_dict1[this_key] = [text]
                    else:
                        result_dict1[this_key].append(text)                

        for i in list(result_dict1.values()):
            result_list1.append(" ".join(i))
        result_dict0[lable_string] = result_list1

    elif lable_string == 'table':
        result_list1 = []
        H,W,row_line = pre_auto_row(crop_img)
        vis = crop_img.copy()
        # cv2.line(vis, (0,0), (W,0), (0,0,0), 1)
        for y in row_line:
            cv2.line(vis, (0,y+2), (W, y+2), (0,0,0), 1)
        cv2.rectangle(vis,(0,0),(W,H),(0,0,0), 1)
        # cv2.imwrite(f'test_dhl/generate_horline{lable_string}.jpg',vis)
        contours = get_box(vis)
        (contours, boundingBoxes) = sort_contours(contours, method="top-to-bottom")
        n1 = 0
        for contour in contours[:len(contours)]:
            n1 += 1
            # area = cv2.contourArea(contour)
            x, y, w, h = cv2.boundingRect(contour)
            new_img = crop_img[y-3:y+h+3, x-3:x+w+3]
            check_img = denoise(new_img)
            check_img = 255-check_img
            # print(f'count_zero: {cv2.countNonZero(check_img)}')
            if cv2.countNonZero(check_img) > 800:
                result_list2 = []
                contours, boundingBoxes = detect_text(new_img)
                n2 = 0
                for contour in contours[:len(contours)]:
                    n2 += 1
                    x, y, w, h = cv2.boundingRect(contour)
                    ratio = w/h
                    new_img2 = new_img[y:y+h, x:x+w]
                    # cv2.imwrite(f"test_dhl/{lable_string}{n1}{n2}.jpg",new_img2)
                    check_img = denoise(new_img2)
                    check_img = 255-check_img
                    if cv2.countNonZero(check_img) > 150:
                        text = ocr_topsarun_api(new_img2)
                        # text = easyocr_reader(new_img2)

                        text = text.replace("\n","").replace("\r","").replace("|","").replace("=","").replace(")","")
                        temp = text
                        if n2 > 1:
                            text = currency_format_f2(text)
                        if text != "":
                            result_list2.append(text)
                        else:
                            try:
                                result_list2[0] += f' {temp}'
                            except:
                                pass
                        # print(text)
                result_list1.append(result_list2)
        
        result_dict0[lable_string] = result_list1        

    elif lable_string == 'table_head' or 'total' in lable_string:
        contours, boundingBoxes = detect_text(crop_img)
        n1 = 0

        if lable_string == 'table_head':
            result_dict0[lable_string] = ['descr', 'net', 'vat', 'gross']
        else:
            result_list = []
            for contour in contours[:len(contours)]:
                n1+= 1
                x, y, w, h = cv2.boundingRect(contour)
                if lable_string == 'sub_total':
                    ratio = -1
                else:
                    ratio = w/h

                new_img = crop_img[y:y+h, x:x+w]
                # cv2.imwrite(f"test_dhl/{lable_string}{n1}.jpg",new_img)
                check_img = denoise(new_img)
                check_img = 255-check_img
                if cv2.countNonZero(check_img) > 80:
                    text = ocr_topsarun_api(new_img)
                    # text = easyocr_reader(new_img)

                    text = text.replace("\n","").replace("\r","").replace("|","").replace(")","").lower()
                    if 'total' in lable_string:
                        text = currency_format_f2(text)
                    result_list.append(text)
                    # print(text)
            if lable_string == 'total_amount' and len(result_list) > 1:
                result_dict0[lable_string] = result_list[len(result_list)-1]
            elif lable_string == 'sub_total' and len(result_list) > 2:
                result_dict0["sub_total_vat"] = result_list[len(result_list)-1]
                result_dict0["sub_total"] = result_list[len(result_list)-2]
            else:
                result_dict0[lable_string] = result_list    


    else:
        check_img = denoise(crop_img)
        check_img = 255-check_img
        if cv2.countNonZero(check_img) > 80:
            # text = easyocr_reader(crop_img)
            text = ocr_topsarun_api_longtext(crop_img,min_size=10,text_threshold=0.5,low_text = 0.3,link_threshold=0.8)
            # print("text:",text)
    
            if 'date' in lable_string:
                match = regex_date(text)
                if match != None:
                    match = match.group(1)
                    text = match
            else:
                if ('no' in lable_string or 'num' in lable_string) and (lable_string != 'orinvno'):
                    text = text.split()
                    text = regex_number(text[-1])
                else:
                    text = text.replace("/","").replace("."," ").replace("-","")
                    if lable_string == "corrrsn":
                        text = text.replace(":","")
                        text = text.split()
                        text = ' '.join(text[4:])
                        text = creditnote_pattern(text)
                    elif lable_string == "orinvno":
                        text = text.replace(":","")
                        text = text.split()
                        text = ' '.join(text[5:])
                    else:
                        text = text.split(':')
                        if len(text) > 1:
                            text = text[1]
                        else:
                            text = text[0]
                    text = regex_number_letter(text)


            # print(text)
            result_dict0[lable_string] = text
    return result_dict0