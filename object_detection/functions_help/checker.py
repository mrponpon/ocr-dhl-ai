import datetime

def validate(date_text):
    try:
        datetime.datetime.strptime(date_text, '%d/%m/%Y')
        return True
    except ValueError:
        return False

def str2float(text):
    return abs(round(float(text.replace(",","")),2))

def check_table(result):
    table = result["table"]
    result_dict = {"CUSTOMS_DUTY":False,"DISRBRURSEMENT":False}
    i = 0
    for t in table:
        try:
            netvat = round(str2float(t["net"]) + str2float(t["vat"]),2)
            gross = str2float(t["gross"])
            # print(a,t["gross"])
            if i == 0:
                descr = "CUSTOMS_DUTY"
            else:
                descr = "DISRBRURSEMENT"
            if netvat==gross:
                result_dict[descr] = True
        except:
            pass
        i += 1
    return result_dict

def calculate_amount(result):
    table = result["table"]
    sub_total = 0
    sub_total_vat = 0
    total_amount = 0
    for t in table:
        net = str2float(t["net"])
        vat = str2float(t["vat"])
        gross = str2float(t["gross"])
        sub_total += net
        sub_total_vat += vat
        total_amount += gross
    return sub_total,sub_total_vat,total_amount

def check_amount(result,result_table):
    result_dict = {"pay_amount":False}
    sub_total = 0
    sub_total_vat = 0
    total_amount = 0
    if result["sub_total"] != "":
        sub_total = str2float(result["sub_total"])
    if result["sub_total_vat"] != "":
        sub_total_vat = str2float(result["sub_total_vat"])
    if result["total_amount"] != "":
        total_amount = str2float(result["total_amount"])

    if result_table["CUSTOMS_DUTY"] and result_table["DISRBRURSEMENT"]:
        table = result["table"]
        temp_dict = {"total_net":0,"total_vat":0,"total_gross":0}
        for t in table:
            net = str2float(t["net"])
            vat = str2float(t["vat"])
            gross = str2float(t["gross"])
            temp_dict["total_net"] += net
            temp_dict["total_vat"] += vat
            temp_dict["total_gross"] += gross

        if ((sub_total == temp_dict["total_net"]) and (sub_total_vat == temp_dict["total_vat"])) or (total_amount == temp_dict["total_gross"]):
            result_dict["pay_amount"] = True

    else:
        if total_amount == sub_total + sub_total_vat:
            result_dict["pay_amount"] = True
    return result_dict

def check_corrrsn(result):
    result_dict = {"corrrsn":False}
    corrrsn = result["corrrsn"]
    if corrrsn != "":
        corrrsn = corrrsn.split()
        if len(corrrsn[0]) != 1 and len(corrrsn[1]) != 4:
            return result_dict
        if corrrsn[0].isnumeric() and (not corrrsn[1][0:2].isnumeric()) and corrrsn[1][2:4].isnumeric():
            result_dict["corrrsn"] = True
    return result_dict

def check_date(result):
    result_dict = {"date":False,"payment_due_date":False}
    date = result["date"]
    payment_due_date = result["payment_due_date"]
    if validate(date):
        result_dict["date"] = True
    if validate(payment_due_date):
        result_dict["payment_due_date"] = True
    return result_dict

def check_result(result):
    result_date = check_date(result)
    if (not result_date["date"]) and result_date["payment_due_date"]:
        result["date"] = result["payment_due_date"]
    if (not result_date["payment_due_date"]) and result_date["date"]:
        result["payment_due_date"] = result["date"]
    result_table = check_table(result)
    print(result_table)
    if result_table["CUSTOMS_DUTY"] and result_table["DISRBRURSEMENT"]:
        sub_total,sub_total_vat,total_amount = calculate_amount(result)
        result["sub_total"] = "-{:.2f}".format(sub_total)
        result["sub_total_vat"] = "-{:.2f}".format(sub_total_vat)
        result["total_amount"] = "-{:.2f}".format(total_amount)
    else:
        if result["total_amount"] == "" and result["sub_total"] != "" and result["sub_total_vat"] != "":
            total_amount = str2float(result["sub_total"]) + str2float(result["sub_total_vat"])
            result["total_amount"] = "-{:.2f}".format(total_amount)
        
    return result