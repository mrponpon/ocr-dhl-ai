# Import packages
import os
import cv2
import numpy as np
import tensorflow as tf
import sys
from starlette.responses import JSONResponse
from pydantic import BaseModel

# This is needed since the notebook is stored in the object_detection folder.
# sys.path.append("..")
from .functions_help import extract_text as ext_text
from .functions_help import checker
from .utils import label_map_util
from .utils import visualization_utils as vis_util
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import json
import base64
from fastapi import FastAPI, File, UploadFile,APIRouter
from multiprocessing.pool import ThreadPool
pool = ThreadPool(processes=15)
import time
import datetime

# Path to frozen detection graph .pb file, which contains the model that is used for object detection.
PATH_TO_CKPT = os.path.join('object_detection','inference_graph_dhlv2','frozen_inference_graph.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join('object_detection','inference_graph_dhlv2','labelmap_dhl.pbtxt')

# Number of classes the object detector can identify
NUM_CLASSES = 15

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    init_op = tf.global_variables_initializer()

    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)
    
# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

router = APIRouter()
class Item(BaseModel):
    img: str

@router.get('/health-check')
def healthCheck():
    image = np.random.randint(255, size=(100,100,3),dtype=np.uint8)
    image_expanded = np.expand_dims(image, axis=0)
    with sess.as_default():
        sess.run(init_op)
        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: image_expanded})
    return JSONResponse(content = {"message":"SUCCESS."}, status_code = 200)

@router.post('/predict')
async def predict_image(img: Item):
    # try:
    date_time = datetime.datetime.now()
    print(date_time)
    image = base64.b64decode(img.img)
    npimg = np.frombuffer(image, np.uint8)
    image = cv2.imdecode(npimg,cv2.IMREAD_COLOR)
    # cv2.imwrite(f'save_img/img_{time.time()}.jpg',image)

    image_copy = image.copy()
    height, width, channels = image.shape
    image_expanded = np.expand_dims(image, axis=0)
    imencoded = cv2.imencode('.jpg', image)[1].tobytes()

    with sess.as_default():
        sess.run(init_op)
        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: image_expanded})       

    # Draw the results of the detection (aka 'visulaize the results')
    _,_,str_box_map = vis_util.visualize_boxes_and_labels_on_image_array(
                                                image,
                                                np.squeeze(boxes),
                                                np.squeeze(classes).astype(np.int32),
                                                np.squeeze(scores),
                                                category_index,
                                                use_normalized_coordinates=True,
                                                max_boxes_to_draw=15,
                                                line_thickness=1,
                                                min_score_thresh=0.3)
    result_dict = {
                "credit_no":'',"payment_due_date":'',"table":[],"orinvno":'',
                "attn":'',"header":'',"date":'',"sub_total_vat":'',
                "sub_total":'',"table_head":[],"acc_no":'',"total_amount":'',
                "postcode":'',"addr":[],"corrrsn":'',"HWB_num":'',"contact_num":''
                }
    check_score = {}
    worker_dict = {}

    for box,lable_string in str_box_map.items():
        checking = True
        score = int(lable_string[0].split(':')[1].strip('%').strip())
        lable_string = lable_string[0].split(':')[0]

        if lable_string in check_score:
            if score > check_score[lable_string]:
                check_score[lable_string] = score
            else:
                checking = False
        else:
            check_score[lable_string] = score

        if checking:
            worker_dict[lable_string] = pool.apply_async(ext_text.extract_text,(image_copy,box,lable_string,height, width,))
                
    for k,v in worker_dict.items():
        result_dict.update(v.get())

    for i in range(len(result_dict["table"])):
        result_dict["table"][i] = dict(zip(result_dict["table_head"], result_dict["table"][i]))

    result_dict = checker.check_result(result_dict)
    # print(result_dict)

            
    imencoded = cv2.imencode('.jpg', image)[1]
    predicted_image = base64.b64encode(imencoded)
    result_dict['predicted_image'] = predicted_image.decode('utf-8')
    return JSONResponse(content = result_dict, status_code = 200)

    # except Exception as e:
    #     print(e)
    #     return JSONResponse(content = {'message' : 'Error'}, status_code = 400)